package ictgradschool.web.lab10.exercise5;

public class UserClass {
    String fName;
    String lName;
    String city;
    String country;

public UserClass(String fName, String lName, String city, String country){
    this.fName = fName;
    this.lName = lName;
    this.city = city;
    this.country = country;
}

    public String getfName() {
        return fName;
    }

    public String getlName() {
        return lName;
    }

    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }
}


