package ictgradschool.web.lab10.exercise04;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class HitCounterServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        Cookie[] cookies = req.getCookies();
        String c = "0";
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("hits")) {
                    c = cookie.getValue() + "";
                }
            }
        }
        c = Integer.parseInt(c) + 1 + "";

        Cookie chocolate = new Cookie("hits", c);

        if ("true".equalsIgnoreCase(req.getParameter("removeCookie"))) {
            //TODO - add code here to delete the 'hits' cookie
            chocolate.setValue("0");
        }
        resp.addCookie(chocolate);

                //TODO - add code here to get the value stored in the 'hits' cookie then increase it by 1 and update the cookie


                //TODO - use the response object's send redirect method to refresh the page
        resp.sendRedirect("hit-counter.html");
    }
}
