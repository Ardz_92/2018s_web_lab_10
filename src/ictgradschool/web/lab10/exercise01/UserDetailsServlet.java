package ictgradschool.web.lab10.exercise01;

import ictgradschool.web.lab10.exercise5.UserClass;
import ictgradschool.web.lab10.utilities.HtmlHelper;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;

public class UserDetailsServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");

        HttpSession sesssion = request.getSession();
        String fName = request.getParameter("fName");
        String lName = request.getParameter("lName");
        String city = request.getParameter("city");
        String country = request.getParameter("country");
        sesssion.setAttribute("user", new UserClass(fName, lName, city, country));
        Cookie firstNameC = new Cookie("fName", URLEncoder.encode(fName, "UTF-8"));
        response.addCookie(firstNameC);
        Cookie lastNameC = new Cookie("lName", URLEncoder.encode(lName, "UTF-8"));
        response.addCookie(lastNameC);
        Cookie cityC = new Cookie("city", URLEncoder.encode(city, "UTF-8"));
        response.addCookie(cityC);
        Cookie countryC = new Cookie("country", URLEncoder.encode(country, "UTF-8"));
        response.addCookie(countryC);

        PrintWriter out = response.getWriter();

        // Header stuff
        out.println(HtmlHelper.getHtmlPagePreamble("Web Lab 10 - Sessions"));

        out.println("<a href=\"index.html\">HOME</a><br>");

        out.println("<h3>Data entered: </h3>");
        //TODO - add the firstName, lastName, city and country  that were entered into the form to the list below
        //TODO - add the parameters from the form to session attributes

//        Cookie[] cookies = request.getCookies();
//        for (Cookie c: cookies){
//        out.print(c.getName() + " " + c.getValue());
//        }

        out.println("<ul>");
        out.println("<li>First Name:" + fName + "</li>");
        out.println("<li>Last Name:" + lName + "</li>");
        out.println("<li>Country:" + country + "</li>");
        out.println("<li>City:" + city + "</li>");
        out.println("</ul>");

        out.println(HtmlHelper.getHtmlPageFooter());
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Process POST requests the same as GET requests
        doGet(request, response);
    }
}
